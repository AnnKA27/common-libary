package ru.kryz.commonlibrary.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class PaymentRequestDTO {
    private UUID userId;
    private UUID orderId;
    private Double amount;
}
