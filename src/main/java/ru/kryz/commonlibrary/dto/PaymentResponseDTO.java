package ru.kryz.commonlibrary.dto;

import lombok.Data;
import ru.kryz.commonlibrary.enums.PaymentStatus;

import java.util.UUID;

@Data
public class PaymentResponseDTO {

    private UUID userId;
    private UUID orderId;
    private Double amount;
    private PaymentStatus status;
}
