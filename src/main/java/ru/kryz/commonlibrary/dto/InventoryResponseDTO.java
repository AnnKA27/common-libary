package ru.kryz.commonlibrary.dto;

import lombok.Data;
import ru.kryz.commonlibrary.enums.InventoryStatus;

import java.util.UUID;

@Data
public class InventoryResponseDTO {

    private UUID orderId;
    private UUID userId;
    private UUID productId;
    private InventoryStatus status;
}
