package ru.kryz.commonlibrary.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class InventoryRequestDTO {

    private UUID userId;
    private UUID productId;
    private UUID orderId;
}
