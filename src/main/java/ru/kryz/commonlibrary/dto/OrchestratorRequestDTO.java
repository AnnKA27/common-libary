package ru.kryz.commonlibrary.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class OrchestratorRequestDTO {

    private UUID userId;
    private UUID productId;
    private UUID orderId;
    private Double amount;
}
