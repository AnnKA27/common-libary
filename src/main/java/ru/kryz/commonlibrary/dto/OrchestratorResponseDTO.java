package ru.kryz.commonlibrary.dto;

import lombok.Data;
import ru.kryz.commonlibrary.enums.OrderStatus;

import java.util.UUID;

@Data
public class OrchestratorResponseDTO {
    private UUID userId;
    private UUID productId;
    private UUID orderId;
    private Double amount;
    private OrderStatus status;
}
