package ru.kryz.commonlibrary.enums;

public enum InventoryStatus {
    AVAILABLE,
    UNAVAILABLE;
}
